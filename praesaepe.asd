;; praesaepe: configuration for presepino
;; Copyright (C) 2018  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(asdf:defsystem :praesaepe
  :description "praesaepe lights"
  :author      "cage"
  :license     "GPLv3"
  :version     "0.0.1"
  :pathname    "src"
  :serial      t
  :depends-on (:alexandria
               :babel
               :parse-number
               :ieee-floats
               :cl-ppcre-unicode
               :cl-i18n
               :cl-colors2
               :ltk
               :flexi-streams
               :marshal
               :cserial-port)
  :components ((:file "package")
               (:file "config")
               (:file "constants")
               (:file "conditions")
               (:file "interfaces")
               (:file "misc-utils")
               (:file "ltk-utils")
               (:file "serial-interface")
               (:file "protocol")
               (:file "led-state")
               (:file "led-editor")
               (:file "static-preview-animation")
               (:file "main-frame")
               (:file "main")))

(progn
  ;; do not push for release
  (pushnew :debug-mode *features*))
