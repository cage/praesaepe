;; praesaepe: configuration for presepino
;; Copyright (C) 2018  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :serial-interface)

(defclass serial-port ()
  ((name
    :initform "/dev/ttyACM0"
    :initarg  :name
    :accessor name)
   (baud-rate
    :initform 9600
    :initarg  :baud-rate
    :accessor baud-rate)
   (data-bits
    :initform 8
    :initarg  :data-bits
    :accessor data-bits)
   (stop-bits
    :initform 1
    :initarg :stop-bits
    :accessor stop-bits)
   (parity
    :initform :none
    :initarg  :parity
    :accessor parity)
   (serial-stream
    :initform nil
    :initarg :serial-stream
    :accessor serial-stream)))

(defparameter *serial-interface* (make-instance 'serial-port))

(defmethod print-object ((object serial-port) stream)
  (print-unreadable-object (object stream :type t :identity t)
    (format stream "~a ~a ~a ~a"
            (baud-rate object)
            (data-bits object)
            (parity    object)
            (stop-bits object))))

(defmethod marshal:class-persistant-slots ((object serial-port))
  '(name baud-rate data-bits stop-bits parity))

(defmethod marshal:initialize-unmarshalled-instance ((object serial-port))
  (let ((unbounded-slots '(serial-stream)))
    (shared-initialize object unbounded-slots))) ;; initialize transients slots

(defgeneric open-port (object))

(defgeneric close-port (object))

(defmethod open-port ((object serial-port))
  (with-accessors ((name      name)
                   (baud-rate     baud-rate)
                   (data-bits     data-bits)
                   (stop-bits     stop-bits)
                   (parity        parity)
                   (serial-stream serial-stream)) object
    (handler-bind ((error
                    #'(lambda(e)
                        (declare (ignore e))
                        (ltk-utils:make-error-box (format nil
                                                          (_ "port ~a can not be opened")
                                                          name))
                        (setf serial-stream nil))))
      (setf serial-stream (open-serial name
                                       :baud-rate baud-rate
                                       :data-bits data-bits
                                       :stop-bits stop-bits
                                       :parity    parity)))))

(defmacro with-valid-port ((serial) &body body)
  `(when (serial-stream ,serial)
     ,@body))

(defmethod close-port ((object serial-port))
  (with-valid-port (object)
    (close-serial (serial-stream object))))

(defmethod read-a-byte ((object serial-port))
  (with-valid-port (object)
    (read-serial-byte (serial-stream object))))

(defmethod write-a-byte ((object serial-port) byte)
  (with-valid-port (object)
    #+debug-mode (misc:dbg "sending ~a 0x~x ~a" byte byte (code-char byte))
    (write-serial-byte byte (serial-stream object))))
