;; praesaepe: configuration for presepino
;; Copyright (C) 2018  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :static-preview-animation)

(defclass preview (toplevel)
  ((canvas
    :initform nil
    :initarg  :canvas
    :accessor canvas)))

(defmethod initialize-instance :after ((object preview) &key &allow-other-keys)
  (with-accessors ((canvas canvas)) object
    (root-title object (_ "Preview"))
    (setf canvas (make-canvas object :width +canvas-w+ :height +min-canvas-h+))
    (pack canvas :fill :both)))

(defun make-preview ()
  (sort-states)
  (let* ((window      (make-instance 'preview))
         (rows-number (all-states-length))
         (hour-font   (font-create "a" :family "Mono" :size (/ +led-h+ 2))))
    (with-accessors ((canvas canvas)) window
      (let* ((canvas-w         (safe-parse-number (cget canvas :width)))
             (led-left-gap     (floor (* canvas-w 0.15)))
             (led-cumulative-w (- canvas-w led-left-gap))
             (led-w            (floor (/ led-cumulative-w *led-numbers*))))
        (configure canvas :height (* +led-h+ rows-number))
        (loop
           for y from 0 by +led-h+
           for state in (all-states-seq) do
             (let* ((leds        (leds state))
                    (items-list  (loop
                                    for x from led-left-gap by led-w
                                    for led in leds collect
                                      (let ((color (as-color-string (color led))))
                                        (list :rectangle
                                              x            y
                                              (+ x led-w) (+ y +led-h+)
                                              :fill color))))
                    (text-handle (ltk:create-text canvas 0 y (hour state))))
               (itemconfigure canvas text-handle :font hour-font)
               (make-items canvas items-list)))
        (font-delete hour-font)
        window))))
