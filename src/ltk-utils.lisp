;; praesaepe: configuration for presepino
;; Copyright (C) 2018  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :ltk-utils)

(defun stop-mainloop ()
  (setf *break-mainloop* t))

(defun restart-mainloop ()
  (setf *break-mainloop* t))

(defun text-width (fontspec text &key (display-of nil))
  (if display-of
      (ltk:format-wish "senddatastring [font measure ~s ~s -displayof ~a]"
                       fontspec text display-of)
      (ltk:format-wish "senddatastring [font measure ~s ~s]" fontspec text))
  (parse-integer (ltk::read-data) :junk-allowed t))

(defun label-measure (fontspec text &key (anchor :center) (master nil))
  "return an alist of width and height of a label in pixel units"

  (let ((label (make-instance 'label :font fontspec
                              :text text :anchor anchor
                              :master master)))
    (ltk:format-wish "puts \[winfo reqwidth ~a\]; flush stdout" (ltk:widget-path label))
    (ltk:format-wish "puts \[winfo reqheight ~a\]; flush stdout" (ltk:widget-path label))
    (ltk:destroy label)
    (list (cons :w (parse-integer (read-line (wish-stream *wish*) :junk-allowed t)))
          (cons :h (parse-integer (read-line (wish-stream *wish*) :junk-allowed t))))))

(defun item-coords (canvas id)
  (ltk:format-wish "senddatastring [~a coords ~a]"
                   (widget-path canvas) id)
  (ltk::read-data))

(defun item-raw-text (canvas text-id)
  (ltk:format-wish "puts [~a itemcget ~a -text]; flush stdout"
                   (widget-path canvas) text-id)
  (read-line (wish-stream *wish*)))

(defun item-hide (canvas handle)
  (ltk:itemconfigure canvas handle :state "hidden"))

(defun item-unhide (canvas handle)
  (ltk:itemconfigure canvas handle :state "normal"))

(defun canvas-position (canvas x y)
  (values (ltk:canvasx canvas x)
          (ltk:canvasy canvas y)))

(defun root-pathname (widget)
  (ltk:format-wish "senddatastring [winfo toplevel ~s]" (widget-path widget))
  (ltk::read-data))

(defun root-title (widget title)
  (let ((root-path (root-pathname widget)))
    (format-wish "wm title ~a {~a}" root-path title)))

(defun make-text-input (prompt &key (initial-text ""))
  (let* ((w (make-instance 'toplevel))
         (*break-mainloop* nil)
         (ok t)
         (l (make-instance 'label :master w :text prompt))
         (e (make-instance 'entry :master w :width 40 :text initial-text))
         (f (make-instance 'frame :master w))
         (b_ok (make-instance 'button :master f :text "Ok"
                              :command #'(lambda () (progn
                                                      (setf ok t)
                                                      (setf *break-mainloop* t)))))

         (b_cancel (make-instance 'button :master f :text "Cancel"
                                  :command (lambda () (progn
                                                        (setf ok nil)
                                                        (setf *break-mainloop* t))))))

    (pack l :side :top :anchor :w)
    (pack e :side :top)
    (pack f :side :top :anchor :e)
    (pack b_cancel :side :right)
    (pack b_ok :side :right)
    (mainloop)
    (ltk:withdraw w)
    (if ok
        (ltk:text e)
        nil)))

(defun format-fontspec (type size &rest options)
  "format a ltk font specification"
  (reduce #'(lambda (x y) (concatenate 'string x " " y))
          (append (list type)
                  options
                  (list (format nil "~d" size)))))

(defun make-info-box (message)
  (message-box message (config:_ "Information") "ok" "info"))

(defun make-error-box (message)
  (message-box message
               (config:_ "Error") "ok" "error"))
