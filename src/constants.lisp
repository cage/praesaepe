;; praesaepe: configuration for presepino
;; Copyright (C) 2018  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :constants)

(define-constant +canvas-w+                        1200     :test #'=)

(define-constant +min-canvas-h+                       40    :test #'=)

(define-constant +led-h+               (- +min-canvas-h+ 2) :test #'=)

(define-constant +dump-element-type+   '(unsigned-byte 8)   :test 'equalp)

(define-constant +dump-file-extension+             "led"    :test 'string=)

(define-constant +serial-config-file-extension+ "serial"    :test 'string=)

(define-constant +dump-type-fading+                    0    :test '=)

(define-constant +dump-type-pattern+                   1    :test '=)

(define-constant +dump-max-states-num+                24    :test '=)

(define-constant +help-about-message-template+
    " ~a
 Copyright (C) 2018  cage

 This program is free software:  you can redistribute it and/or modify
 it under the terms of the  GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 This program is  distributed in the hope that it  will be useful, but
 WITHOUT  ANY   WARRANTY;  without   even  the  implied   warranty  of
 MERCHANTABILITY or  FITNESS FOR  A PARTICULAR  PURPOSE.  See  the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program."
  :test #'string=)
