;; praesaepe: configuration for presepino
;; Copyright (C) 2018  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(defpackage :config
  (:use :cl
        :alexandria
        :cl-i18n)
  (:export
   :+program-name+
   :+wish-bin+
   :_
   :n_
   :*led-numbers*
   :*color-led-pattern*))

(defpackage :constants
  (:use :cl
        :alexandria)
  (:export
   :+canvas-w+
   :+min-canvas-h+
   :+led-h+
   :+dump-element-type+
   :+dump-file-extension+
   :+serial-config-file-extension+
   :+dump-type-pattern+
   :+dump-type-fading+
   :+dump-max-states-num+
   :+help-about-message-template+))

(defpackage :interfaces
  (:use :cl
        :alexandria
        :config
        :constants)
  (:export
   :clone
   :clone-into
   :with-simple-clone
   :dump
   :as-color-string
   :serialize-to-stream
   :serialize
   :deserialize
   :read-a-byte
   :write-a-byte))

(defpackage :conditions
  (:use :cl)
  (:export
   :text-error
   :text
   :not-implemented-error
   :null-reference
   :out-of-bounds
   :length-error
   :different-length-error
   :xml-no-matching-tag
   :xml-no-such-attribute
   :invalid-aabb-error
   :invalid-texture
   :with-default-on-error))

(defpackage :misc-utils
  (:use :cl
        :alexandria
        :parse-number
        :config
        :constants
        :interfaces)
  (:nicknames :misc)
  (:export
   :slurp-file
   :put-in-clipboard
   :get-from-clipboard
   :format-fn-symbol
   :make-file-glob
   :dbg
   :safe-parse-number
   :safe-delete@
   :safe-subseq
   :fresh-list-insert@
   :return-nil
   :return-whole
   :new-index
   :rgb=
   :fn-delay
   :stable-multisort
   :stable-multisort*
   :gen-multisort-test
   :universal-time-list
   :time-second-of
   :time-minutes-of
   :time-hour-of
   :time-date-of
   :time-month-of
   :time-year-of
   :time-day-of
   :time-daylight-p-of
   :time-zone-of))

(defpackage :ltk-utils
  (:use :cl
        :alexandria
        :ltk)
  (:export
   :root-title
   :make-info-box
   :make-error-box))

(defpackage :serial-interface
  (:use
   :cl
   :alexandria
   :cserial-port
   :ltk
   :config
   :constants
   :misc
   :ltk-utils
   :interfaces)
  (:export
   :*serial-interface*
   :serial-port
   :name
   :baud-rate
   :data-bits
   :stop-bits
   :parity
   :stream
   :open-port
   :close-port))

(defpackage :protocol
  (:use
   :cl
   :alexandria
   :config
   :constants
   :misc
   :interfaces
   :serial-interface)
  (:export
   :+set-clock+
   :+hello+
   :+test-color+
   :+store-data+
   :+allocate-memory+
   :send-set-clock
   :send-hello
   :send-test-color
   :make-16bit-number
   :split-16bit-number
   :send-allocate-memory
   :send-data
   :send-get-temperature))

(defpackage :led-state
  (:use
   :cl
   :alexandria
   :ltk
   :cl-colors2
   :config
   :constants
   :misc
   :ltk-utils
   :interfaces)
  (:export
   :current-state-display-fx
   :set-current-state-display-fx
   :get-current-state
   :get-state@
   :inc-current-state
   :dec-current-state
   :clean-states-db
   :push-state
   :all-states-seq
   :all-states-length
   :sort-states
   :delete-current-state
   :duplicate-current-state
   :insert-state-@current-index
   :use-fading-fx-p
   :led
   :color
   :gui-tag
   :state
   :leds
   :hour
   :display-fx
   :gui-canvas
   :gui-hour
   :->hsv-client
   :->hsv-led
   :hsv-led-h
   :hsv-led-s
   :hsv-led-v
   :make-leds
   :make-state
   :make-state*
   :canvas-led-tag
   :draw-leds-strip
   :sync-gui-to-state))

(defpackage :led-editor
  (:use
   :cl
   :alexandria
   :cl-ppcre
   :ltk
   :cl-colors2
   :config
   :constants
   :misc
   :ltk-utils
   :interfaces)
  (:export
   :editor
   :bound-led
   :bound-state))

(defpackage :main-frame
  (:use
   :cl
   :alexandria
   :ltk
   :cl-colors2
   :config
   :constants
   :led-state
   :misc
   :ltk-utils
   :interfaces)
  (:export
   :main-frame
   :canvas
   :frame-top
   :frame-bottom
   :hour-label
   :hour-entry
   :fading-mode
   :pattern-mode
   :ok-button
   :go-left
   :go-right
   :add-state
   :delete-state
   :state
   :draw-leds-strip))

(defpackage :static-preview-animation
  (:use
   :cl
   :alexandria
   :ltk
   :cl-colors2
   :config
   :constants
   :led-state
   :misc
   :ltk-utils
   :interfaces)
  (:export
   :make-preview))

(defpackage :main
  (:use
   :cl
   :alexandria
   :ltk
   :cl-colors2
   :config
   :constants
   :led-state
   :misc
   :ltk-utils
   :interfaces)
  (:export))
