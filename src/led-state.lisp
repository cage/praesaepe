;; praesaepe: configuration for presepino
;; Copyright (C) 2018  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :led-state)

(defstruct states-group
  (states       '())
  (idx          -1))

(defparameter *states* (make-states-group))

(defun all-states-seq ()
  (states-group-states *states*))

(defun all-states-length ()
  (length (all-states-seq)))

(defun all-states-idx ()
  (states-group-idx *states*))

(defun clean-states-db ()
  (setf (states-group-states *states*) '()))

(defun push-state (state)
  (setf (states-group-states *states*)
        (concatenate 'list (all-states-seq) (list state)))
  (setf (states-group-idx *states*) (1- (length (all-states-seq))))
  state)

(defun get-current-state ()
  (if (or (<  (all-states-idx) 0)
          (>= (all-states-idx) (length (all-states-seq))))
      nil
      (elt (all-states-seq) (all-states-idx))))

(defun inc-current-state ()
  (setf (states-group-idx *states*) (rem (1+ (all-states-idx)) (length (all-states-seq))))
  (get-current-state))

(defun dec-current-state ()
  (decf (states-group-idx *states*))
  (when (< (all-states-idx) 0)
    (setf (states-group-idx *states*) (1- (length (all-states-seq)))))
  (get-current-state))

(defun get-state@ (idx)
  (elt (all-states-seq) idx))

(defun insert-state-@current-index (new-state)
  (if (or (null (all-states-seq))
          (=    (all-states-idx)
                (1- (all-states-length))))
      (push-state new-state)
      (setf (states-group-states *states*)
            (misc:fresh-list-insert@ (all-states-seq) new-state (all-states-idx)))))

(defun delete-current-state ()
  (handler-bind ((error (invoke-restart (find-restart 'misc:return-nil))))
    (when-let ((new-states (misc:safe-delete@ (all-states-seq) (all-states-idx)))
               (old-idx    (states-group-idx *states*))
               (old-length (all-states-length)))
      (when (= old-idx (1- old-length))
        (decf (states-group-idx *states*)))
      (setf (states-group-states *states*) new-states))))

(defun duplicate-current-state ()
  (handler-bind ((error (invoke-restart (find-restart 'misc:return-nil))))
    (when-let* ((current-state (get-current-state))
                (new-state     (clone current-state)))
      (insert-state-@current-index new-state))))

(defun current-state-display-fx ()
  (display-fx (get-current-state)))

(defun set-current-state-display-fx (v)
  (let ((current-state (get-current-state)))
    (setf (display-fx current-state) v)
    current-state))

(defun sort-state-hour< (a b)
  (< (hour a) (hour b)))

(defun sort-state-hour> (a b)
  (> (hour a) (hour b)))

(defun sort-state-type< (a b)
  (cond
    ((and (use-fading-fx-p a)
          (use-fading-fx-p b))
     t)
    ((use-fading-fx-p a) ; just a
     t)
    ((use-fading-fx-p b) ; just b
     nil)
    (t                   ; neither a nor b
     nil)))

(defun sort-state-type> (a b)
  (cond
    ((and (not (use-fading-fx-p a))
          (not (use-fading-fx-p b)))
     nil)
    ((use-fading-fx-p a) ; just a
     t)
    ((use-fading-fx-p b) ; just b
     nil)
    (t                   ; neither a nor b
     nil)))

(defun sort-states ()
  (let ((sorted (stable-multisort* (all-states-seq)
                                   (gen-multisort-test sort-state-hour<
                                                       sort-state-hour>
                                                              identity)
                                   (gen-multisort-test sort-state-type<
                                                       sort-state-type>
                                                       identity))))
    (setf (states-group-states *states*) sorted)))

(defclass led ()
  ((color
    :initform (as-hsv +black+)
    :initarg  :color
    :accessor color
    :type     hsv)
   (gui-tag
    :initform ""
    :initarg  :gui-tag
    :accessor gui-tag
    :type     string
    :documentation "The ltk tag (for binding, callback etc.)")))

(defmethod as-color-string ((object led))
  (print-hex-rgb (color object)))

(defmethod as-color-string ((object hsv))
  (print-hex-rgb object))

(defmethod clone ((object hsv))
  (copy-hsv object))

(defmethod clone-into :after ((from led) (to led))
  (setf (color   to) (clone    (color   from))
        (gui-tag to) (copy-seq (gui-tag from)))
  to)

(defmethod clone ((object led))
  (with-simple-clone (object 'led)))

(defgeneric ->hsv-led (object)
  (:documentation "remap hsv in a format suitable for the led strip library on the MCU"))

(defmethod ->hsv-led ((object hsv))
  (vector (round (* (/ (hsv-hue object) 360)
                    255))
          (round (* (hsv-saturation object) 255))
          (round (* (hsv-value object)      255))))

(defmethod ->hsv-led ((object led))
  (->hsv-led (color object)))

(defmacro gen-acc-hsv-client (suffix pos)
  `(defun ,(format-fn-symbol t "hsv-led-~a" suffix) (hsv)
     (elt hsv ,pos)))

(gen-acc-hsv-client h 0)

(gen-acc-hsv-client s 1)

(gen-acc-hsv-client v 2)

(defmethod ->hsv-client (h s v)
  "the dual of ->hsv-led"
  (hsv (* (/ h 255) 360)
       (/ s 255)
       (/ v 255)))

(defmethod dump ((object led) stream &key &allow-other-keys)
  (write-sequence (->hsv-led (color object)) stream))

(defgeneric use-fading-fx-p (object))

(defclass state ()
  ((leds
    :initform '()
    :initarg  :leds
    :accessor leds)
   (hour
    :initform nil
    :initarg  :hour
    :accessor hour)
   (display-fx
    :initform +dump-type-fading+
    :initarg  :display-fx
    :accessor display-fx)
   (main-frame
    :initform nil
    :initarg  :main-frame
    :accessor main-frame
    :allocation :class)))

(defmethod print-object ((object state) stream)
  (print-unreadable-object (object stream :type t :identity t)
    (format stream "h ~a" (hour object))))

(defmethod clone-into :after ((from state) (to state))
  (setf (leds       to) (mapcar #'clone (leds from))
        (hour       to)                 (hour from)
        (display-fx to)           (display-fx from))
  to)

(defmethod clone ((object state))
  (with-simple-clone (object 'state)))

(defmethod use-fading-fx-p ((object state))
  (= (display-fx object) +dump-type-fading+))

(defmethod dump ((object state) stream &key &allow-other-keys)
  (write-byte (hour         object) stream)
  (write-byte (display-fx object) stream)
  (dolist (led (leds object))
    (dump led stream)))

(defun make-leds (&key (color (as-hsv +black+)))
  (loop for i from 0 below  *led-numbers* collect
       (let ((led-instance  (make-instance 'led
                                           :color color)))
         led-instance)))

(defun make-state (main-frame)
  (let ((state (make-instance 'state
                              :leds           (make-leds)
                              :main-frame    main-frame)))
    state))

(defun make-state* ()
  "Note: main window  are shared between instances of 'state'"
  (let ((state (make-instance 'state
                              :leds (make-leds))))
    state))

(defun canvas-led-tag (x y)
  (format nil "~a-~a" x y))

(defun led-on-p (led)
  (rgb= (led-state:color led) *color-led-pattern*))

(defun make-led-edit-cb (led state)
  #'(lambda (e)
      (declare (ignore e))
      (if (use-fading-fx-p state)
          (make-instance 'led-editor:editor
                         :bound-led   led
                         :bound-state state)
          (progn ; pattern mode
            (if (led-on-p led)
                (setf (color led) (as-hsv +black+))
                (setf (color led) *color-led-pattern*))
            (draw-leds-strip state)))))

(defun make-led-copy-cb (led)
  #'(lambda (e)
      (declare (ignore e))
      (put-in-clipboard (clone led))))

(defun make-led-paste-cb (led state)
  #'(lambda (e)
      (declare (ignore e))
      (let ((led-clipboard (get-from-clipboard)))
        (when led-clipboard
          (setf (color led)
                (clone (color led-clipboard)))
          (draw-leds-strip state)))))

(defun draw-leds-strip (state &key (y 0))
  (let ((led-w  (floor (/ +canvas-w+ *led-numbers*)))
        (canvas (gui-canvas state)))
    (loop for x from 0 below +canvas-w+ by led-w
          for led in (leds state) do
         (let* ((gui-led         (create-rectangle canvas
                                                   x y
                                                   (+ x led-w) (+ y +led-h+)))
                (led-tag         (canvas-led-tag x y))
                (led-color       (color led))
                (gui-led-outline (hsv (hsv-hue        led-color)
                                      (hsv-saturation led-color)
                                      (- 1.0 (hsv-value led-color)))))
           (setf (gui-tag led) led-tag)
           (itemconfigure canvas gui-led "fill"    (as-color-string led))
           (itemconfigure canvas gui-led "outline" (as-color-string gui-led-outline))
           (itemconfigure canvas gui-led "tag"  led-tag)
           (tagbind canvas led-tag "<ButtonPress-1>" (make-led-edit-cb led state))
           (tagbind canvas led-tag "<ButtonPress-2>" (make-led-copy-cb led))
           (tagbind canvas led-tag "<ButtonPress-3>" (make-led-paste-cb led state))))))

(defgeneric gui-canvas (object))

(defgeneric gui-display-fx (object))

(defgeneric gui-hour (object))

(defmethod gui-canvas ((object state))
  (main-frame:canvas (main-frame object)))

(defmethod gui-display-fx ((object state))
  (main-frame:fading-mode (main-frame object)))

(defmethod gui-hour ((object state))
  (main-frame:hour-entry (main-frame object)))

(defun sync-gui-to-state (state)
  (with-accessors ((display-fx display-fx)
                   (hour       hour)) state
    (draw-leds-strip state)
    (setf (value (gui-display-fx state)) display-fx)
    (setf (text  (gui-hour       state)) hour)))
