;; praesaepe: configuration for presepino
;; Copyright (C) 2018  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :protocol)

(define-constant +set-clock+                         #\C :test #'char=)

(define-constant +hello+                             #\H :test #'char=)

(define-constant +test-color+                        #\T :test #'char=)

(define-constant +store-data+                        #\D :test #'char=)

(define-constant +allocate-memory+                   #\A :test #'char=)

(define-constant +extension+                         #\X :test #'char=)

(define-constant +extension-reply+                   #\X :test #'char=)

(define-constant +extension-temp+                    #\T :test #'char=)

(define-constant +command-terminator+         #\Linefeed :test #'char=)

(define-constant +ok-reply+                          #\K :test #'char=)

(define-constant +error-reply+                       #\E :test #'char=)

(define-constant +error-unexpected+                   -1 :test #'=)

(defun read-a-line (stream &optional (accum (make-array 0 :adjustable t :fill-pointer 0)))
  (let ((v (read-a-byte stream)))
    (if (char-equal (code-char v) +command-terminator+)
        accum
        (progn
          (vector-push-extend v accum)
          (read-a-line stream accum)))))

(defun wait-reply (stream &key (get-raw-reply-p nil))
  (let ((reply (read-a-line stream)))
    #+debug-mode
    (misc:dbg "reply as hex : ~{0x~x ~}~%" (map 'list #'identity  reply))
    (misc:dbg "reply as char: ~{~a~}~%" (map 'list #'code-char reply))
    (cond
      ((= (length reply) 0)
       +error-unexpected+)
      ((char-equal (code-char (first-elt reply)) +ok-reply+)
       nil)
      ((char-equal (code-char (first-elt reply)) +error-reply+)
       (elt reply 1))
      (t
       (if get-raw-reply-p
           reply
           (wait-reply stream))))))

(defun send-command (stream prefix &rest parameters)
  (write-a-byte stream (char-code prefix))
  (loop for i in parameters do
       (write-a-byte stream i))
  (write-a-byte stream (char-code +command-terminator+))
  (let ((reply (wait-reply stream)))
    (when reply
      (error 'conditions:text-error :text (format nil "~a" reply)))))

(defun send-extension-command (stream prefix &rest parameters)
  (write-a-byte stream (char-code +extension+))
  (write-a-byte stream (char-code prefix))
  (loop for i in parameters do
       (write-a-byte stream i))
  (write-a-byte stream (char-code +command-terminator+))
  (let ((reply (wait-reply stream :get-raw-reply-p t)))
    (if (char= +extension-reply+
               (code-char (first-elt reply)))
        (safe-subseq reply 1)
        (error 'conditions:text-error :text (format nil "~a" reply)))))

(defun send-set-clock (stream hour)
  #+debug-mode (assert (and (>= hour  0)
                            (<  hour 23)))
  (send-command stream +set-clock+ hour))

(defun send-hello (stream)
  (send-command stream +hello+))

(defun send-test-color (stream h s v)
  "h s v are octect 0-255 range"
  (send-command stream +test-color+ h s v))

(defun make-16bit-number (a b)
  (let ((mib (ash a 8)))
    (logior mib b)))

(defun split-16bit-number (a)
  (let ((lib (logior (logand a #x00ff)
                     #x0000))
        (mib (logior (ash a -8) 0)))
    (values mib lib)))

(defun send-allocate-memory (stream size)
  (multiple-value-bind (a b)
      (split-16bit-number size)
    (send-command stream +allocate-memory+ a b)))

(defun send-data (stream data)
  (send-command stream +store-data+ data))

;;;; extension

(defun send-get-temperature (stream)
  (send-extension-command stream +extension-temp+))
