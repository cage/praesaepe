;; praesaepe: configuration for presepino
;; Copyright (C) 2018  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :misc-utils)

(defparameter *clipboard* nil)

(defun put-in-clipboard (v)
  (setf *clipboard* v))

(defun get-from-clipboard ()
  *clipboard*)

(defmacro format-fn-symbol (package format &rest format-args)
  `(alexandria:format-symbol ,package ,(concatenate 'string "~:@(" format "~)")
                             ,@format-args))

(defmacro dbg (f &rest a)
  `(format t (concatenate 'string ,f "~%") ,@a))


(defun make-file-glob (extension)
  (assert (stringp extension))
  (concatenate 'string "*." extension))

(defun safe-parse-number (number &key (fix-fn #'(lambda (e) (declare (ignore e)) nil)))
  (handler-bind ((parse-error
                  #'(lambda(e)
                      (return-from safe-parse-number (funcall fix-fn e))))
                 (invalid-number
                  #'(lambda(e)
                      (return-from safe-parse-number (funcall fix-fn e)))))
    (if (or (not (stringp number))
            (string= number "-"))
        nil
        (parse-number number))))

(defun eps= (a b &optional (epsilon 1e-10))
  (<= (abs (- a b)) epsilon))

(defun rgb= (rgb1 rgb2 &optional (epsilon 1e-5))
  "Compare RGB colors for (numerical) equality."
  (let ((rgb-1 (cl-colors2:as-rgb rgb1))
        (rgb-2 (cl-colors2:as-rgb rgb2)))
    (and (eps= (cl-colors2:rgb-red   rgb-1) (cl-colors2:rgb-red   rgb-2) epsilon)
         (eps= (cl-colors2:rgb-green rgb-1) (cl-colors2:rgb-green rgb-2) epsilon)
         (eps= (cl-colors2:rgb-blue  rgb-1) (cl-colors2:rgb-blue  rgb-2) epsilon))))

(defgeneric delete@ (sequence position))

(defgeneric safe-delete@ (sequence position)
  (:documentation "Return sequence if position is out of bound"))

(defmacro gen-delete@ ((sequence position) &body body)
  `(if (and (>= ,position 0)
            (< ,position (length ,sequence)))
       ,@body
      (error 'conditions:out-of-bounds :seq sequence :idx position)))

(defmethod delete@ ((sequence list) position)
  (gen-delete@
   (sequence position)
   (append (subseq sequence 0 position)
           (and (/= position (- (length sequence) 1))
                (subseq sequence (1+ position))))))

(defmethod delete@ ((sequence vector) position)
  (gen-delete@
   (sequence position)
    (make-array (1- (length sequence))
                :fill-pointer (1- (length sequence))
                :adjustable t
                :initial-contents (concatenate 'vector (subseq sequence 0 position)
                                               (and (/= position (- (length sequence) 1))
                                                    (subseq sequence (1+ position)))))))

(defmethod safe-delete@ ((sequence sequence) position)
  (restart-case
      (delete@ sequence position)
    (return-nil () nil)
    (return-whole () sequence)
    (new-index (i) (safe-delete@ sequence i))))

(defun safe-subseq (sequence start &optional (end nil))
  "return the whole sequence if end is beyond the length of the sequence"
  (subseq sequence
          start
          (and end (min end (length sequence)))))

(defmacro *cat (type-return input)
  `(reduce #'(lambda (a b) (concatenate ',type-return a b)) ,input))

(defun lcat (&rest v)
  (*cat list v))

(defun vcat (&rest v)
  (*cat vector v))

(defun fresh-list-insert@ (a v pos)
  (lcat (subseq a 0 pos)
        (list v)
        (subseq a pos)))

(defmacro fn-delay (a)
  (if (symbolp a)
      `(lambda (&rest p) (apply (function ,a) p))
      `(lambda (&rest p) (apply ,a p))))

(defun stable-multisort (bag fns)
  (stable-sort bag
               #'(lambda (a b)
                   (let ((partial (loop named outer for fn in fns do
                                       (cond
                                         ((< (funcall fn a b) 0)
                                          (return-from outer t))
                                         ((> (funcall fn a b) 0)
                                          (return-from outer nil))))))
                     partial))))

(defun stable-multisort* (bag &rest fns)
  (stable-multisort bag fns))

(defmacro gen-multisort-test (fn-< fn-> fn-access)
  (alexandria:with-gensyms (a b access-a access-b)
    `(lambda (,a ,b)
       (let ((,access-a (funcall (fn-delay ,fn-access) ,a))
             (,access-b (funcall (fn-delay ,fn-access) ,b)))
         (cond
           ((funcall (fn-delay ,fn-<) ,access-a ,access-b)
            -1)
           ((funcall (fn-delay ,fn->) ,access-a ,access-b)
            1)
           (t 0))))))

(defun slurp-file (filename &key (convert-to-string t))
  "A simple way to slurp a file."
  (with-open-file (stream filename :direction :input :element-type '(unsigned-byte 8))
    (let ((seq (make-array (file-length stream) :element-type '(unsigned-byte 8))))
      (read-sequence seq stream)
      (if convert-to-string
          (babel:octets-to-string seq)
          seq))))

(defmacro gen-time-access (name pos)
  `(defun ,(format-fn-symbol t "time-~a-of" name) (time-list)
     (elt time-list ,pos)))

(defmacro gen-all-time-access (&rest name-pos)
  `(progn
     ,@(loop for i in name-pos collect
            `(gen-time-access ,(car i) ,(cdr i)))))

(defun universal-time-list ()
  (multiple-value-list (get-decoded-time)))

(gen-all-time-access (second     . 0)
                     (minutes    . 1)
                     (hour       . 2)
                     (date       . 3)
                     (month      . 4)
                     (year       . 5)
                     (day        . 6)
                     (daylight-p . 7)
                     (zone       . 8))
