;; praesaepe: configuration for presepino
;; Copyright (C) 2018  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :main-frame)

(defclass main-frame (frame)
  ((canvas
    :initform nil
    :initarg  :canvas
    :accessor canvas)
   (frame-top
    :initform (make-instance 'frame)
    :initarg  :frame-top
    :accessor frame-top)
   (frame-bottom
    :initform (make-instance 'frame)
    :initarg  :frame-bottom
    :accessor frame-bottom)
   (hour-label
    :initform nil
    :initarg  :hour-label
    :accessor hour-label)
   (hour-entry
    :initform nil
    :initarg  :hour-entry
    :accessor hour-entry)
   (fading-mode
    :initform nil
    :initarg  :fading-mode
    :accessor fading-mode)
   (pattern-mode
    :initform nil
    :initarg  :pattern-mode
    :accessor pattern-mode)
   (ok-button
    :initform nil
    :initarg  :ok-button
    :accessor ok-button)
   (go-left
    :initform nil
    :initarg  :go-left
    :accessor go-left)
   (go-right
    :initform nil
    :initarg  :go-right
    :accessor go-right)
   (add-state
    :initform nil
    :initarg  :add-state
    :accessor add-state)
   (delete-state
    :initform nil
    :initarg  :delete-state
    :accessor delete-state)
   (duplicate-state
    :initform nil
    :initarg  :duplicate-state
    :accessor duplicate-state)
   (show-preview
    :initform nil
    :initarg  :show-preview
    :accessor show-preview)
   (state
    :initform nil
    :initarg  :state
    :accessor state)))

(defmethod initialize-instance :after ((object main-frame) &key &allow-other-keys)
  (with-accessors ((canvas          canvas)
                   (frame-top       frame-top)
                   (frame-bottom    frame-bottom)
                   (hour-label      hour-label)
                   (hour-entry      hour-entry)
                   (fading-mode     fading-mode)
                   (pattern-mode    pattern-mode)
                   (ok-button       ok-button)
                   (go-left         go-left)
                   (go-right        go-right)
                   (add-state       add-state)
                   (delete-state    delete-state)
                   (duplicate-state duplicate-state)
                   (show-preview    show-preview)
                   (state           state)) object
    (root-title object (_ "LED strip configurator"))
    (setf canvas       (make-canvas nil :width +canvas-w+ :height +min-canvas-h+))
    (setf hour-label   (make-instance 'label
                                      :master frame-top
                                      :text   (_"hour:")))
    (setf hour-entry   (make-instance 'entry
                                      :master frame-top
                                      :width  10
                                      :text   (_ "0 to 23")))
    (setf fading-mode  (make-instance 'radio-button
                                      :master   frame-top
                                      :text     (_ "Fading mode")
                                      :variable "mode"
                                      :value    +dump-type-fading+))
    (setf pattern-mode (make-instance 'radio-button
                                      :master   frame-top
                                      :text     (_ "Pattern mode")
                                      :variable "mode"
                                      :value    +dump-type-pattern+))
    (setf ok-button    (make-instance 'button
                                      :master  frame-bottom
                                      :text    (_ "Accept state")
                                      :command (make-set-hour-state-fn)))
    (setf go-left      (make-instance 'button
                                      :master  frame-bottom
                                      :text    (_ "Prev. state")
                                      :command #'(lambda ()
                                                   (sync-gui-to-state (dec-current-state)))))
    (setf go-right     (make-instance 'button
                                      :master  frame-bottom
                                      :text    (_ "Next state")
                                      :command #'(lambda ()
                                                   (sync-gui-to-state (inc-current-state)))))
    (setf add-state     (make-instance 'button
                                       :master  frame-bottom
                                       :text    (_ "Add state")
                                       :command #'add-new-state-command))
    (setf delete-state  (make-instance 'button
                                       :master frame-bottom
                                       :text   (_ "Delete state")
                                       :command #'delete-state-command))
    (setf duplicate-state  (make-instance 'button
                                       :master frame-bottom
                                       :text   (_ "Duplicate state")
                                       :command #'duplicate-state-command))
    (setf show-preview  (make-instance 'button
                                       :master  frame-bottom
                                       :text    (_ "Preview")))
    (setf state                  (make-state object))
    (setf (command show-preview)  #'(lambda ()
                                      (static-preview-animation:make-preview)))
    (initialize-menu hour-entry)
    (setf (command fading-mode)  #'mode-command)
    (setf (command pattern-mode) #'mode-command)
    (push-state state)
    (setf (value fading-mode) +dump-type-fading+)
    (configure-frame-bottom-columns-grid frame-top 5)
    (draw-leds-strip state)
    (grid hour-label   0 0  :sticky :we)
    (grid hour-entry   0 1  :sticky :w :columnspan 2)
    (grid fading-mode  0 3  :sticky :w)
    (grid pattern-mode 0 4  :sticky :e)
    (grid frame-top    0 0  :sticky :we)
    (grid canvas     1 0)
    (configure-frame-bottom-columns-grid frame-bottom 10)
    (grid ok-button       0 0 :sticky :w :columnspan 2)
    (grid go-left         0 2 :sticky :e)
    (grid go-right        0 3 :sticky :w :columnspan 2)
    (grid add-state       0 5 :sticky :we)
    (grid duplicate-state 0 6 :sticky :we)
    (grid delete-state    0 7 :sticky :w :columnspan 2)
    (grid show-preview    0 9 :sticky :e)
    (grid frame-bottom    2 0 :sticky :we)))

(defun make-load-dump-completed-messages (states-number)
  (make-info-box (format nil (_ "Succesfully loaded ~a strip states") states-number)))

(defun make-load-dump-command (hour-entry)
  #'(lambda ()
      (let ((file (get-open-file :filetypes (list (list (_ "Led files")
                                                        (make-file-glob +dump-file-extension+))))))
        (when (and file
                   (uiop:file-exists-p file))
          (with-open-file (stream file
                                  :direction    :input
                                  :element-type +dump-element-type+)
            (flet ((read-a-byte ()
                     (read-byte stream)))
            (let* ((state-num  (read-byte stream))
                   (led-number (read-byte stream))
                   (all-states (loop repeat state-num collect
                                    (make-state*))))
              ;; call this for first! the expressions after depend of a
              ;; correct value of *led-numbers*
              (setf *led-numbers* led-number)
              (clean-states-db)
              (loop for state in all-states do
                   (push-state state))
              (loop for state-idx from 0 below state-num do ; read each state
                   (let ((state (get-state@ state-idx)))
                     (setf (hour       state) (read-a-byte))
                     (setf (display-fx state) (read-a-byte))
                     (loop for led-idx from 0 below *led-numbers* do
                          (let* ((h         (read-a-byte))
                                 (s         (read-a-byte))
                                 (v         (read-a-byte))
                                 (new-color (->hsv-client h s v))
                                 (led       (elt (leds state) led-idx)))
                            (setf (color led) new-color)))))
              (setf (text hour-entry) (hour (get-current-state)))
              (draw-leds-strip (get-current-state))
              (make-load-dump-completed-messages (length all-states)))))))))

(defun hour-range-entry-valid-p (new-hour)
  (and new-hour
       (integerp new-hour)
       (>= new-hour 0)
       (<  new-hour 24)))

(defun make-save-dump-command ()
  #'(lambda ()
      (sort-states)
      (let ((file (get-save-file :filetypes (list (list (_ "Led files") "*.led")))))
        (when (and file
                   (not (uiop:directory-exists-p file)))
          (if (hour-range-entry-valid-p (hour (get-current-state)))
              (with-open-file (stream file
                                      :direction         :output
                                      :element-type      +dump-element-type+
                                      :if-exists         :supersede
                                      :if-does-not-exist :create)
                (write-sequence (dump-to-vector) stream))
              (make-hour-invalid-message))))))

(defun menu-copy-state-command ()
  (put-in-clipboard (clone (get-current-state))))

(defun menu-paste-state-command ()
  (when-let ((state (clone (get-from-clipboard))))
    (insert-state-@current-index state)
    (sync-gui-to-state (get-current-state))
    (draw-leds-strip state)))

(defun menu-help-about-command ()
  (with-modal-toplevel (toplevel :master nil :title (_ "About"))
    (let* ((editor  (make-text toplevel)))
      (setf (text editor)
	    (format nil +help-about-message-template+ +program-name+))
      (pack editor))))

(defun load-dump-serial-command (name baud-rate data-bits stop-bits parity-bits)
  #'(lambda ()
      (let ((file (get-open-file :filetypes
                                 (list (list (_ "Serial config files")
                                             (make-file-glob +serial-config-file-extension+))))))
        (when (and file
                   (uiop:file-exists-p file))
          (setf serial-interface:*serial-interface*
                (deserialize (make-instance 'serial-interface:serial-port)
                             file))
          (sync-serial-port name baud-rate data-bits stop-bits parity-bits)))))

(defun serial-parity-string->keyword (s)
  (cond
    ((string= s "N")
     :none)
    ((string= s "E")
     :even)
    ((string= s "O")
     :odd)
    ((string= s "M")
     :mark)
    ((string= s "S")
     :space)
    (t
     :none)))

(defun sync-serial-port (name baud-rate data-bits stop-bits parity-bits)
  (setf (text name)        (serial-interface:name      serial-interface:*serial-interface*)
        (text baud-rate)   (serial-interface:baud-rate serial-interface:*serial-interface*)
        (text data-bits)   (serial-interface:data-bits serial-interface:*serial-interface*)
        (text stop-bits)   (serial-interface:stop-bits serial-interface:*serial-interface*)
        (text parity-bits) (serial-interface:parity    serial-interface:*serial-interface*)))

(defun save-serial-port-command (name baud-rate data-bits stop-bits parity-bits)
  #'(lambda ()
      (let* ((baud-rate    (safe-parse-number (text baud-rate)))
             (data-bits    (safe-parse-number (text data-bits)))
             (stop-bits    (safe-parse-number (text stop-bits)))
             (text-parity  (text parity-bits))
             (text-name    (text name))
             (parity-bits  (serial-parity-string->keyword text-parity)))
        (if (and text-name
                 baud-rate
                 data-bits
                 stop-bits)
            (let ((file (get-save-file :filetypes
                                       (list (list (_ "Serial config files")
                                                   (make-file-glob +serial-config-file-extension+))))))
              (when file
                (with-open-file (stream file
                                        :direction         :output
                                        :if-exists         :supersede
                                        :if-does-not-exist :create)
                  (serialize-to-stream (make-instance 'serial-interface:serial-port
                                                      :name      text-name
                                                      :baud-rate baud-rate
                                                      :data-bits data-bits
                                                      :stop-bits stop-bits
                                                      :parity    parity-bits)
                                       stream))))
            (make-error-box (_ "Parameters not valids"))))))

(defun connect-serial-port-command ()
  (serial-interface:open-port serial-interface:*serial-interface*)
  (make-info-box (_ "Connection established")))

(defun menu-serial-port-command ()
  (with-modal-toplevel (toplevel :master nil :title (_ "Serial configuration"))
    (let* ((name-lb        (make-instance 'label  :master toplevel :text (_ "Port name")))
           (baud-rate-lb   (make-instance 'label  :master toplevel :text (_ "Baud rate")))
           (data-bits-lb   (make-instance 'label  :master toplevel :text (_ "Data bits")))
           (stop-bits-lb   (make-instance 'label  :master toplevel :text (_ "Stop bits")))
           (parity-bits-lb (make-instance 'label  :master toplevel :text (_ "Parity bits")))
           (name           (make-instance 'entry  :master toplevel :text "/dev/ttyACM0"))
           (baud-rate      (make-instance 'entry  :master toplevel :text "9600"))
           (data-bits      (make-instance 'entry  :master toplevel :text "8"))
           (stop-bits      (make-instance 'entry  :master toplevel :text "1"))
           (parity-bits    (make-instance 'entry  :master toplevel :text "N"))
           (connect-bt     (make-instance 'button :master toplevel :text (_ "Connect")
                                          :command  #'connect-serial-port-command))
           (save-bt        (make-instance 'button :master toplevel :text (_ "Save")
                                          :command (save-serial-port-command name
                                                                             baud-rate
                                                                             data-bits
                                                                             stop-bits
                                                                             parity-bits)))
           (load-bt        (make-instance 'button :master toplevel :text (_ "Load")
                                          :command
                                          (load-dump-serial-command name
                                                                    baud-rate
                                                                    data-bits
                                                                    stop-bits
                                                                    parity-bits))))
      (pack name-lb)
      (pack name)
      (pack baud-rate-lb)
      (pack baud-rate)
      (pack data-bits-lb)
      (pack data-bits)
      (pack stop-bits-lb)
      (pack stop-bits)
      (pack parity-bits-lb)
      (pack parity-bits)
      (pack connect-bt)
      (pack save-bt)
      (pack load-bt))))

(defmacro with-modal-message-on-error ((msg) &body body)
  `(handler-case
       (progn ,@body)
     (conditions:text-error (a)
       (make-error-box (format nil ,msg (conditions:text a))))))

(defun menu-ping-serial-command ()
  (with-modal-message-on-error ((_ "Can not communicate with device: ~a."))
    (protocol:send-hello serial-interface:*serial-interface*)
    (make-info-box (_ "Pong ;)"))))

(defun set-clock-serial-command ()
  (with-modal-message-on-error ((_ "Error setting clock: ~a."))
    (let ((hour (time-hour-of (universal-time-list))))
      (protocol:send-set-clock serial-interface:*serial-interface* hour)
      (make-info-box (format nil (_ "Set clock for device at ~a") hour)))))

(defun send-dump-serial-command ()
  (with-modal-message-on-error ((_ "Error sending animation."))
    (let ((dump (dump-to-vector)))
      (protocol:send-allocate-memory serial-interface:*serial-interface* (length dump))
      (map nil
           #'(lambda (a) (protocol:send-data serial-interface:*serial-interface* a))
           dump)
      (make-info-box (_ "Loading terminated")))))

(defun get-temperature-command ()
  (with-modal-message-on-error ((_ "Error getting temperature."))
    (let ((temperature (protocol:send-get-temperature serial-interface:*serial-interface*)))
      (make-info-box (format nil (_ "Temperature is: ~a") temperature)))))

(defun initialize-menu (hour-entry)
  (let* ((bar    (make-menubar))
	 (file   (make-menu bar (_ "File")))
         (edit   (make-menu bar (_ "Edit")))
         (comm   (make-menu bar (_ "Comunication")))
         (help (make-menu bar (_ "Help"))))
    (make-menubutton file (_ "Load animation")
                     (make-load-dump-command hour-entry)                      :underline 0)
    (make-menubutton file (_ "Serial port")     #'menu-serial-port-command    :underline 0)
    (make-menubutton file (_ "Save")            (make-save-dump-command)      :underline 0)
    (make-menubutton edit (_ "Copy")            #'menu-copy-state-command     :underline 0)
    (make-menubutton edit (_ "Paste")           #'menu-paste-state-command    :underline 0)
    (make-menubutton comm (_ "Connect")         #'connect-serial-port-command :underline 0)
    (make-menubutton comm (_ "Ping device")     #'menu-ping-serial-command    :underline 0)
    (make-menubutton comm (_ "Set clock")       #'set-clock-serial-command    :underline 5)
    (make-menubutton comm (_ "Send Animation")  #'send-dump-serial-command    :underline 5)
    (make-menubutton comm (_ "Get temperature") #'get-temperature-command     :underline 0)
    (make-menubutton help (_ "About")           #'menu-help-about-command     :underline 0)))

(defun make-hour-invalid-message ()
  (make-error-box (_ "The hour must be an integer between 0 and 23")))

(defun make-set-hour-state-fn ()
  #'(lambda ()
      (when-let* ((state    (get-current-state))
                  (new-hour (handler-case
                                (parse-integer (text (gui-hour state)))
                              (error () -1))))
        (if (hour-range-entry-valid-p new-hour)
            (setf (hour state) new-hour)
            (progn
              (make-hour-invalid-message)
              nil)))))

(defun add-new-state-command ()
  ;; save current status and check saving gone well
  (when (funcall (make-set-hour-state-fn))
      (let ((new-state (make-state*)))
        (sync-gui-to-state new-state)
        (push-state        new-state))))

(defun delete-state-command ()
  (when (ask-yesno (_ "Really delete this state?") :title (_ "Question"))
    (delete-current-state)
    (sync-gui-to-state (get-current-state))))

(defun duplicate-state-command ()
  (duplicate-current-state)
  (sync-gui-to-state (get-current-state)))

(defun dump-to-vector ()
  (flexi-streams:with-output-to-sequence (stream :element-type +dump-element-type+)
    (write-byte (min (all-states-length) +dump-max-states-num+) stream)
    (write-byte *led-numbers* stream)
    (loop for state in (all-states-seq) do
         (dump state stream))))

(defun configure-frame-bottom-columns-grid (frame &optional (columns-number 6))
  (loop for i from 0 below columns-number do
       (grid-columnconfigure frame i :uniform "u")))

(defun mode-command (val)
  (set-current-state-display-fx val))
