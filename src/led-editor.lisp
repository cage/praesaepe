;; praesaepe: configuration for presepino
;; Copyright (C) 2018  cage

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(in-package :led-editor)

(define-constant +preview-size+ 128 :test #'=)

(defclass editor (toplevel)
  ((bound-led
    :initform nil
    :initarg  :bound-led
    :accessor bound-led)
   (saved-bound-led
    :initform nil
    :initarg  :saved-bound-led
    :accessor saved-bound-led)
   (preview
    :initform nil
    :initarg  :preview
    :accessor preview)
   (preview-as-string
    :initform nil
    :initarg  :preview-as-string
    :accessor preview-as-string)
   (bound-state
    :initform nil
    :initarg  :bound-state
    :accessor bound-state)))

(defmacro gen-set-component-led  (name acc-h acc-s acc-v)
  (with-gensyms (old new-hsv v l)
    `(defun ,(format-fn-symbol t "~a" name) (,l ,v)
       (let* ((,old     (led-state:color ,l))
              (,new-hsv (hsv ,(if (null acc-h) v `(,acc-h ,old))
                             ,(if (null acc-s) v `(,acc-s ,old))
                             ,(if (null acc-v) v `(,acc-v ,old)))))
         (setf (led-state:color ,l) ,new-hsv)))))

(gen-set-component-led set-h-led nil hsv-saturation hsv-value)

(gen-set-component-led set-s-led hsv-hue nil hsv-value)

(gen-set-component-led set-v-led hsv-hue hsv-saturation nil)

(defun hsv->string-for-human (color)
  (format nil "~6,2f,~4,2f,~4,2f"
          (hsv-hue        color)
          (hsv-saturation color)
          (hsv-value      color)))

(defun string-for-human->hsv (color)
  (let ((components (split "," color)))
    (if (= (length components) 3)
        (let ((h (safe-parse-number (elt components 0)))
              (s (safe-parse-number (elt components 1)))
              (v (safe-parse-number (elt components 2))))
          (if (and h s v)
              (hsv h s v)
              nil))
        nil)))

(defun make-scale-command (editor led-color-accessor-fn)
  #'(lambda (a)
      (with-accessors ((led               bound-led)
                       (preview           preview)
                       (preview-as-string preview-as-string)) editor
        (funcall led-color-accessor-fn led a)
        (setf (text preview-as-string) (hsv->string-for-human (led-state:color led)))
        (configure preview "bg" (as-color-string led)))))

(defun make-cancel-command (led saved-led state)
  #'(lambda ()
      (setf (led-state:color led)
            (led-state:color saved-led))
      (main-frame:draw-leds-strip state)))

(defun make-scale-h-command (scale text-entry)
  #'(lambda ()
      (setf (text text-entry) (value scale))))

(defun make-ok-command (state led color-text-preview preview scale-h scale-s scale-v)
  #'(lambda ()
      (let ((color (string-for-human->hsv (text color-text-preview))))
        (when color
          (setf (led-state:color led) color)
          (configure preview "bg" (as-color-string led))
          (setf (value scale-h) (hsv-hue        color))
          (setf (value scale-s) (hsv-saturation color))
          (setf (value scale-v) (hsv-value      color))
          (main-frame:draw-leds-strip state)
          ;; send color to led strip
          (let ((hsv-for-led (led-state:->hsv-led led)))
            (protocol:send-test-color serial-interface:*serial-interface*
                                      (led-state:hsv-led-h hsv-for-led)
                                      (led-state:hsv-led-s hsv-for-led)
                                      (led-state:hsv-led-v hsv-for-led)))))))

(defmethod initialize-instance :after ((object editor) &key &allow-other-keys)
  (with-accessors ((preview           preview)
                   (preview-as-string preview-as-string)
                   (bound-led         bound-led)
                   (saved-bound-led   saved-bound-led)
                   (bound-state bound-state)) object
    (root-title object (_ "Edit LED"))
    (setf saved-bound-led (clone bound-led))
    (let* ((label-head    (make-instance 'label  :master object :text (_ "Edit led")))
           (h-label       (make-instance 'label  :master object :text (_ "hue:")))
           (s-label       (make-instance 'label  :master object :text (_ "saturation:")))
           (v-label       (make-instance 'label  :master object :text (_ "value:")))
           (h             (make-instance 'scale  :master object :from 0.0 :to 360.0))
           (s             (make-instance 'scale  :master object :from 0.0 :to 1.0))
           (v             (make-instance 'scale  :master object :from 0.0 :to 1.0))
           (ok-button     (make-instance 'button :master object :text (_ "OK")))
           (cancel-button (make-instance 'button :master object :text (_ "cancel")
                                          :command (make-cancel-command bound-led
                                                                        saved-bound-led
                                                                        bound-state))))
      (setf preview (make-canvas object :width +preview-size+ :height +preview-size+))
      (setf preview-as-string
            (make-instance 'entry
                           :master object
                           :text   (hsv->string-for-human (led-state:color bound-led))))
      (configure preview "bg" (as-color-string bound-led))
      (setf (command ok-button) (make-ok-command bound-state bound-led
                                                 preview-as-string
                                                 preview h s v))
      (pack label-head   :side :top)
      (pack h-label)
      (setf (command h) (make-scale-command object #'set-h-led))
      (pack h)
      (pack s-label)
      (setf (command s) (make-scale-command object #'set-s-led))
      (pack s)
      (pack v-label)
      (setf (command v) (make-scale-command object #'set-v-led))
      (pack v)
      (pack preview)
      (pack preview-as-string)
      (pack ok-button)
      (pack cancel-button))))
